# URL SHORTENER

### Steps to follow for Linux
- git clone git@bitbucket.org:kerimlcr/url-shortener.git
- cd url-shortener/
- python3 -m venv .venv
- source .venv/bin/activate
- pip install -r req.txt
- python fff.py

### Steps to follow for Windows
- git clone git@bitbucket.org:kerimlcr/url-shortener.git
- cd url-shortener/
- virtualenv.exe .venv
- .venv\Scripts\activate
- pip install -r req.txt
- python fff.py

### Default Superuser
```
# Don't forget to change it later
username= admin
email = admin@admin.com
password= 1234
```