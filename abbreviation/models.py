from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


# Create your models here.
class UrlAbbreviation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    source_url = models.CharField(max_length=200)
    destination_url = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.created_date)

    class Meta:
        verbose_name_plural = "Yeni kısa Url oluştur"


class ClickForDate(models.Model):
    abb = models.ForeignKey(UrlAbbreviation, on_delete=models.CASCADE)
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.abb.user) + "-" + str(self.abb.source_url)

    class Meta:
        verbose_name_plural = "Tıklanma Bilgileri"
