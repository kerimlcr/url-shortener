from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(label='Username', max_length=32)
    password = forms.CharField(label='Password', max_length=32)


class CreateAbbreviationForm(forms.Form):
    source = forms.CharField(label='source', max_length=255, required=True)
