from .views import statistics, create_abbreviation, redirect, homepage, authentication
from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView


urlpatterns = [
    path('', homepage.HomepageView.as_view(), name='homepage'),

    # Redirect pattern
    path(r'r/<str:token>/', redirect.redirect_to_url, name='redirect_url'),

    # Login, Logout, Register patterns
    path(r'login/', LoginView.as_view(template_name='index.html'), name='login'),
    path(r'logout/', LogoutView.as_view(), name='logout'),
    path(r'register/', authentication.Register.as_view(), name='register'),


    # Abbreviation patterns
    path(r'create-abbreviation', create_abbreviation.create_abbreviation, name='create_abbreviation'),
    path(r'statistics', statistics.StatisticsView.as_view(), name='statistics')
]
