from django.apps import AppConfig


class AbbreviationConfig(AppConfig):
    name = 'abbreviation'
