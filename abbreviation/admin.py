from django.contrib import admin
from .models import UrlAbbreviation

# Register your models here.
admin.site.register(UrlAbbreviation)
