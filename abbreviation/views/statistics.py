from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView
from ..models import ClickForDate
from django.db.models.functions import TruncDay
from django.db.models import Count


class StatisticsView(LoginRequiredMixin, ListView):
    model = ClickForDate
    template_name = 'abb-statistics.html'
    context_object_name = 'Statistics'

    def get_context_data(self, **kwargs):
        context = super(StatisticsView, self).get_context_data(**kwargs)
        obj = self.get_queryset().filter(abb__user__id=self.request.user.id).values()\
            .annotate(day=TruncDay('created_date')).values('day')\
            .annotate(count=Count('id')).values('day', 'count', 'abb__destination_url', 'abb__source_url')
        context['obj'] = obj
        return context
