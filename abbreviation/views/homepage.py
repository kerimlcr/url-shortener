from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView
from ..models import UrlAbbreviation


class HomepageView(LoginRequiredMixin, ListView):
    model = UrlAbbreviation
    template_name = 'homepage.html'

    def get_queryset(self):
        return self.model.objects.filter(user__id=self.request.user.id)
