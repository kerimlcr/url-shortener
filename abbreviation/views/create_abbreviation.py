from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from ..forms import CreateAbbreviationForm
from ..models import UrlAbbreviation
import short_url


@login_required
def create_abbreviation(request):
    if request.method == 'POST':
        form = CreateAbbreviationForm(request.POST)
        if form.is_valid():
            source = form.cleaned_data['source']
            if 'http' in source:
                source = source.split("//")[1]
            obj = UrlAbbreviation(
                user=request.user,
                source_url=source
            )
            obj.save()
            obj.destination_url = short_url.encode_url(obj.id)
            obj.save()
        return render(request, 'abbreviation.html', {'url': obj.destination_url})
    return render(request, 'abbreviation.html')
