from django.http import HttpResponseRedirect
from ..models import UrlAbbreviation, ClickForDate


def redirect_to_url(request, token):
    """
    Gelen token doğrultusunda yönlendirmeyi sağlayan fonksiyon.
    :param request request:
    :param str token:
    :return:
    """
    destination = UrlAbbreviation.objects.all().filter(destination_url=token).values('source_url', 'id')
    obj = ClickForDate()
    obj.abb = UrlAbbreviation.objects.get(destination_url=token)
    obj.save()
    return HttpResponseRedirect("https://" + destination[0]['source_url'])
