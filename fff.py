import os

# Kullanılan sistem belirlenir
operation_system = os.name

# Her iki sistemde aynı şekilde çalışlan komutlar
os.system("python manage.py makemigrations && python manage.py migrate")
os.system("python manage.py makemigrations abbreviation && python manage.py migrate abbreviation")

if operation_system == 'nt':
    os.system("""echo from django.contrib.auth.models import User;\
    User.objects.create_superuser('admin', 'a@b.com', '1234') | python manage.py shell""")
elif operation_system == 'posix':
    os.system("""echo "from django.contrib.auth.models import User;\
    User.objects.create_superuser('admin', 'admin@admin.com', '1234')" | python manage.py shell""")
else:
    print("Beklenmedik bir hata meydana geldi, lütfen gerekli işlemleri manual yapınız.")
